package stjepan.bmi_calculator;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.opengl.EGLDisplay;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CalculateActivity extends Activity implements View.OnClickListener{

    private static final String TAG = "Stjepan";
    Button btnCalculate;
    EditText etHeight, etWeight;
    TextView tvBMIResult, tvBMIResultDescription;
    ImageView imBMIResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculate);
        initializeUI();
    }

    private void initializeUI(){
        this.btnCalculate = (Button) findViewById(R.id.btnCalculate);
        this.btnCalculate.setOnClickListener(this);

        this.etHeight = (EditText) findViewById(R.id.etHeight);
        this.etWeight = (EditText) findViewById(R.id.etWeight);
        this.imBMIResult = (ImageView) findViewById(R.id.ivBMIResult);
        this.tvBMIResult = (TextView) findViewById(R.id.tvBMIResult);
        this.tvBMIResultDescription = (TextView) findViewById(R.id.tvBMIResultDescription);
    }
    @Override
    public void onClick(View v) {
        double height = 0.0;
        double weight = 0.0;
        try{
            height = Double.valueOf(this.etHeight.getText().toString());
            weight = Double.valueOf(this.etWeight.getText().toString());

            checkHeightInput(height);
            checkWeightInput(weight);

            showBMI(height, weight);
        }catch (Exception e1)
        {
            Toast.makeText(this, "Bad input!", Toast.LENGTH_SHORT).show();
            resetEditTexts();
            e1.printStackTrace();
            Log.d(TAG, "Error");
        }
    }

    private void resetEditTexts() {
        this.etHeight.setText("");
        this.etWeight.setText("");
    }

    private void showBMI(double height, double weight) {
        double bmi = weight / (height * height);
        String result = "", resultDescription = "";
        if (bmi <= 18.50){
            this.imBMIResult.setBackgroundResource(R.drawable.bmi_underweight);
            result = "Underweight";
            resultDescription = "You should start eating, like NOW!!!";
        }
        else if(bmi >= 18.50 && bmi < 24.99){
            this.imBMIResult.setBackgroundResource(R.drawable.bmi_normal);
            result = "Normal";
            resultDescription = "Keep up the good work!";

        }
        else if(bmi >= 25.00 && bmi < 30.0){
            this.imBMIResult.setBackgroundResource(R.drawable.bmi_overweight);
            result = "Overweight";
            resultDescription = "Lay off the chocolate and chips!";

        }
        else if (bmi >= 30.0){
            this.imBMIResult.setBackgroundResource(R.drawable.bmi_obese);
            result = "Obese";
            resultDescription = "Lay off EVERYTHING and start eating salads!!!";

        }
        result = result.concat("\nIndex: ").concat(String.valueOf(bmi));
        this.tvBMIResult.setText(result);
        this.tvBMIResultDescription.setText(resultDescription);
    }

    private boolean checkHeightInput(double height) throws InputException{
        Log.d(TAG, "provjera heighta" + String.valueOf(height));
        if (height <= 0.0 || height > 2.5){
            throw new InputException();
        }
        return true;
    }
    private boolean checkWeightInput(double weight) throws InputException{
        Log.d(TAG, "Provjera weighta" + String.valueOf(weight));
        if (weight <= 0.0 || weight > 350.0){
            throw new InputException();
        }
        return true;
    }
}

class InputException extends Exception{
    public String errorMsg = "Bad input";
}
